﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuisBot
{
    [Serializable]
    public class PolicyDetails
    {
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyReportNumber { get; set; }
        public string strDoc { get; set; }
        public string strDoc1;
        public string StrDoc
        { get {  return this.strDoc; } set { this.strDoc = value; } }

        public string getDoc(ref string  str)
        {
            return  str;
        }

    }
}