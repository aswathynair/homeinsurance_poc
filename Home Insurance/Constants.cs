﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuisBot
{
    public static class Constants
    {
        public static string FirstEntity = "First greetings";
        public static string Claim = "Claims";
        public static string PhoneNumber = "builtin.phonenumber";
        public static string PolicyNumber = "policynumber";
        public static string InvalidPolicyNumber = "InvalidPolicyNumber";
        public static string InvalidMobileNumber = "InvalidPhoneNumber";
        public static string Help = "help";
        public static string HelpContent = "HelpText";
        public static string IncidentTheft = "theft";
        public static string PoliceComplaint = "PoliceCompalint";
        public static string IsPoliceComplaint = "IsPoliceCompliant";
        public static string IsNoPoliceComplaint = "NoPoliceComplaint";
        public static string AskForPoliceComplaintNumber = "AskForPoliceComplaintNumber";
        public static string PolicyAvailable = "PolicyAvailable";
        public static string PoliceReportNumber = "PoliceReportNumber";
        public static string ValidPhoneNumber = "ValidPhoneNumber";
        public static string ValidPolicyNumber = "ValidPolicyNumber";
        public static string OptionSelect = "OptionSelect";
        public static string IncidentLoss = "loss";
        public static string IncidentDamaged = "damaged";
        public static string IncidentStolen = "stolen";

        public static string IncidentDate = "IncidentDate";
        public static string IncidentLocation = "IncidentLocation";
        public static string InsuredLocation = "InsuredLocation";
        public static string DamageEstimate = "DamageEstimate";
        public static string ClaimStatus = "ClaimStatus";
        public static string WaitMessage = "WaitMessage";

        public static string Claim1 = "Claims1";
        public static string FurtherDamage = "FurtherDamage";
        public static string HurtSomeone = "HurtSomeone";
        public static string IncidentStuff = "IncidentStuff";
        public static string IncidentNature = "IncidentNature";
        public static string ClaimProcess = "ClaimProcess";
        public static string IncidentDetails = "IncidentDetails";
        public static string ScanPolicReport = "ScanPolicReport";
        public static string IncidentType = "IncidentType";
        public static string ScanReceipts = "ScanReceipts";
        public static string BankAccountNumber = "BankAccountNumber";
        public static string Thankyou = "Thankyou";

        public static string MyStuff = "My stuff";

        public static string ImageQnA = "qwerty";
        public static string ScanImage = "ScanImage";



        public static string Shopping = "shopping";
        public static string Buy = "buy";
        public static string BuySucess = "BuySucess";
        public static string GetEmail = "getEmail";
        public static string ExchangeSucess = "ExchangeSucess";
        public static string EmailtIdFailure = "EmailtIdFailure";
        public static string ExchangeReason = "ExchangeReason";
        public static string BillUpload = "BillUpload";


    }
}