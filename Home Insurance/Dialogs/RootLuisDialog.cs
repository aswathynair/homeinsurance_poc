﻿namespace LuisBot.Dialogs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Builder.FormFlow;
    using Microsoft.Bot.Builder.Luis;
    using Microsoft.Bot.Builder.Luis.Models;
    using Microsoft.Bot.Connector;
    using System.IO;
    using LumenWorks.Framework.IO.Csv;
    using System.Net;
    using Newtonsoft.Json;
    using QnAMakerDialog;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using System.Text;
    using System.Data;

    //[LuisModel("a996a109-a79a-4a02-9b27-cdaad8464c20", "270649c69deb4c2c9549a2908decf1ce",Staging =true)]
    //[LuisModel("288a6a37-b586-4221-a60b-43a099020083", "270649c69deb4c2c9549a2908decf1ce", Staging = true)]
    //[LuisModel("288a6a37-b586-4221-a60b-43a099020083", "270649c69deb4c2c9549a2908decf1ce")]
    //https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/a27b2d35-6d86-479e-811f-6a0027acc73d?subscription-key=f1fac7d123c1452aa90dfda56aad8d93&verbose=true&timezoneOffset=0&q=

    //[LuisModel("a27b2d35-6d86-479e-811f-6a0027acc73d", "d7be64e0191745d49b6e9b12f83303ec")]
    [LuisModel("a27b2d35-6d86-479e-811f-6a0027acc73d", "f1fac7d123c1452aa90dfda56aad8d93")]
    [Serializable]
    public class RootLuisDialog : LuisDialog<object>
    {
        public string Message { get; set; }

        public string InsuredName { get; set; }
        public string PolicyNumber { get; set; }
        public string IncidentDate { get; set; }
        public string IncidentLocation { get; set; }
        public string InsuredPremises { get; set; }
        public string EstimatedDamage { get; set; }
        PolicyDetails mc = new PolicyDetails();
        public RootLuisDialog()
        {
            Message = string.Empty;
            //Message = "Hi"; // string.Empty;

        }
        [LuisIntent("")]
        [LuisIntent("None")]
        public async Task None(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            var askedForPhonenumber = context.UserData.GetValue<bool>("AskedForMobileNumber");
            var username = context.UserData.GetValue<string>("Name");
            Message = string.Format(Constants.Help, result.Query);
            var message = await activity;
            if (result.Entities != null)
            {
                foreach (var item in result.Entities)
                {
                    if (item.Type == Constants.PhoneNumber)
                    {
                        var user = await GetuserDetails("Mobile Number", message.Text);
                        if (user != null)
                        {
                            if (!string.IsNullOrEmpty(user.Name))
                            {
                                context.UserData.SetValue("Name", user.Name);
                                context.UserData.SetValue("MobileNumber", user.MobileNumber);
                                context.UserData.SetValue("PolicyNumber", user.PolicyNumber);
                                Message = string.Format(GetMessageFromQnAMaker(Constants.PhoneNumber), user.Name);
                                context.UserData.SetValue("AskedForPolicyNumber", true);
                            }
                        }
                        else
                        {
                            Message = GetMessageFromQnAMaker(Constants.InvalidMobileNumber);
                        }
                    }
                }
            }
            await context.PostAsync(Message);
            context.Wait(this.MessageReceived);
        }

        [LuisIntent("Greetings")]
        public async Task Search(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            List<string> lstSplitArray = new List<string>();
            Activity actvtyWithButtons = (Activity)context.MakeMessage();
            string strTimePeriod = "", strSplitSentnce = "";
            var message = await activity;
            Regex regex = new Regex("</br>");
            int curTimeHour = DateTime.Now.Hour;


            if (curTimeHour < 12)
            {
                strTimePeriod = "Good morning";
            }
            else if (curTimeHour > 12 && curTimeHour < 16)
            {
                strTimePeriod = "Good afternoon";
            }
            else
            {
                strTimePeriod = "Good evening";
            }
            foreach (var item in result.Entities)
            {
                if (item.Type == Constants.FirstEntity)
                {
                    Message = String.Format(GetMessageFromQnAMaker(Constants.FirstEntity), strTimePeriod);

                    Message = String.Format(GetMessageFromQnAMaker(Constants.FirstEntity), "Kevin");
                    

                }
                
            }

            var splitSentance = regex.Split(Message);
            strSplitSentnce = splitSentance[0] + splitSentance[1];
            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
            await context.PostAsync(actvtyWithButtons);
            context.Wait(this.MessageReceived);
            //break;
            //ImageViewer viewer = new ImageViewer(); //create an image viewer
            //Capture capture = new Capture(); //create a camera captue
            //Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
            //{  //run this until application closed (close button click on image viewer)
            //    viewer.Image = capture.QueryFrame(); //draw the image obtained from camera
            //});
            //viewer.ShowDialog(); //show the image viewer
        }

        [LuisIntent("InsurranceType")]
        public async Task Reviews(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            var message = await activity;
            string strSplitSentnce = "";
            Regex regex = new Regex("</br>");
            EntityRecommendation mandClaim = new EntityRecommendation();
            mandClaim.Entity = Constants.Claim;
            Activity actvtyWithButtons = (Activity)context.MakeMessage();

            if (result.Entities != null && result.Entities.Count > 0)
            {
                if (context.UserData.ContainsKey("AskedGreetings") || result.Entities[0].Entity.ToUpper() == mandClaim.Entity.ToUpper())
                {
                    foreach (var item in result.Entities)
                    {
                        if (item.Type == Constants.Claim)
                        {
                            Message = GetMessageFromQnAMaker(Constants.Claim1);
                            if (!context.UserData.ContainsKey("AskedGreetings"))
                            {
                                context.UserData.SetValue("AskedGreetings", true);
                            }
                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);

                            context.UserData.SetValue("AskForPoliceComplaintNumber", false);

                            break;


                        }
                        else if (item.Type == Constants.PhoneNumber)
                        {
                            Message = string.Format(GetMessageFromQnAMaker(Constants.IncidentStuff));

                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);
                            context.UserData.SetValue("AskedForMobileNumber", true);
                        }
                        //if (context.UserData.ContainsKey("AskedForMobileNumber") && !context.UserData.ContainsKey("AskedForPolicyNumber"))
                        //{
                        //    if (item.Type == Constants.PhoneNumber)
                        //    {
                        //        var user = await GetuserDetails("Mobile Number", message.Text);
                        //        if (user != null)
                        //        {
                        //            if (!string.IsNullOrEmpty(user.Name))
                        //            {
                        //                context.UserData.SetValue("Name", user.Name);
                        //                context.UserData.SetValue("MobileNumber", user.MobileNumber);
                        //                context.UserData.SetValue("PolicyNumber", user.PolicyNumber);
                        //                PolicyNumber = user.PolicyNumber;
                        //                Message = string.Format(GetMessageFromQnAMaker(Constants.PhoneNumber), user.Name);
                        //                context.UserData.SetValue("AskedForPolicyNumber", true);
                        //                context.UserData.SetValue("AskedGreetings", true);
                        //                break;
                        //            }
                        //        }
                        //        else
                        //        {

                        //            Message = GetMessageFromQnAMaker(Constants.InvalidMobileNumber);
                        //            break;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        Message = GetMessageFromQnAMaker(Constants.ValidPhoneNumber);
                        //        break;
                        //    }
                        //}
                        //if ((context.UserData.ContainsKey("AskedForMobileNumber") && context.UserData.ContainsKey("MobileNumber")) || context.UserData.ContainsKey("PolicyNumber"))
                        //{
                        //    if (item.Type == Constants.PolicyNumber)
                        //    {
                        //        var policy = context.UserData.GetValue<string>("PolicyNumber");
                        //        if (policy == message.Text)
                        //        {

                        //            Message = GetMessageFromQnAMaker(Constants.PolicyNumber);
                        //            var splitSentance = regex.Split(Message);
                        //            strSplitSentnce = splitSentance[0] + splitSentance[1] + splitSentance[2];
                        //            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                        //            context.UserData.SetValue("AskForPoliceComplaintNumber", false);

                        //            await context.PostAsync(actvtyWithButtons);
                        //            context.Wait(this.MessageReceived);
                        //            return;
                        //        }
                        //        else
                        //        {
                        //            Message = GetMessageFromQnAMaker(Constants.InvalidPolicyNumber);
                        //        }

                        //    }
                        //    else if (context.UserData.ContainsKey("AskForPoliceComplaintNumber") && item.Type == Constants.PoliceReportNumber)
                        //    {
                        //        context.UserData.SetValue("AskForPoliceComplaintNumber", true);
                        //        Message = GetMessageFromQnAMaker(Constants.PolicyAvailable);
                        //        resetContext(context);
                        //    }
                        //    else
                        //    {
                        //        Message = GetMessageFromQnAMaker(Constants.ValidPolicyNumber);

                        //    }
                        //}
                        //else
                        //{
                        //    Message = GetMessageFromQnAMaker(Constants.ValidPhoneNumber);
                        //}
                    }
                }
                else
                {
                    Message = GetMessageFromQnAMaker(Constants.HelpContent);
                    resetContext(context);
                }
            }
            //else
            //{
            //    Message = GetMessageFromQnAMaker(Constants.ValidPolicyNumber);
            //}
            if (Message.Length > 0)
            {
                await context.PostAsync(Message);
                context.Wait(this.MessageReceived);
            }
        }

        [LuisIntent("Help")]
        public async Task Help(IDialogContext context, LuisResult result)
        {
            Message = GetMessageFromQnAMaker(Constants.HelpContent);
            await context.PostAsync(Message);
            context.Wait(this.MessageReceived);
        }

        [LuisIntent("Incident")]
        public async Task Incidents(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            string strSplitSentnce = "";
            Activity actvtyWithButtons = (Activity)context.MakeMessage();
            Regex regex = new Regex("</br>");
            try
            {
                var message = await activity;
                if (context.UserData.ContainsKey("AskedGreetings"))
                {
                    //userData.SetProperty<bool>("AskForFurtherDamage", false);

                    //if (context.UserData.ContainsKey("AskedForMobileNumber") && context.UserData.ContainsKey("Name"))
                    //{
                    //    if (context.UserData.ContainsKey("PolicyNumber"))
                    //    {
                    //        PolicyNumber = context.UserData.GetValue<string>("PolicyNumber");
                    //        var name = context.UserData.GetValue<string>("Name");
                    //        InsuredName = name;


                    if (result.Entities.Count > 1)
                    {
                        result.Entities[0].Entity.Remove(0);
                        //LuisResult result1= result.Entities[0].Entity.Remove(0);
                    }
                    foreach (var item in result.Entities)
                    {

                        if (context.UserData.GetValue<bool>("AskForPoliceComplaintNumber") == false && item.Type == Constants.IsNoPoliceComplaint)
                        {
                            Message = string.Format(GetMessageFromQnAMaker(Constants.FurtherDamage));

                            //var splitSentance = regex.Split(Message);
                            //strSplitSentnce = splitSentance[0] + splitSentance[1];
                            //actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            //Message = "";
                            //await context.PostAsync(actvtyWithButtons);
                            //context.Wait(this.MessageReceived);
                            context.UserData.SetValue("AskForFurtherDamage", true);


                        }
                        else if (item.Type == Constants.FurtherDamage)
                        {
                            Message = string.Format(GetMessageFromQnAMaker(Constants.HurtSomeone));

                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        }
                        else if (context.UserData.GetValue<bool>("AskForPoliceComplaintNumber") == false && context.UserData.GetValue<bool>("AskForFurtherDamage") == true && item.Type == Constants.IsNoPoliceComplaint)
                        {
                            Message = string.Format(GetMessageFromQnAMaker(Constants.IncidentStuff));

                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);
                        }
                        else if (item.Type == Constants.IncidentStuff)
                        {
                            Message = string.Format(GetMessageFromQnAMaker(Constants.IncidentNature));

                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        }
                        else if (item.Type == Constants.IncidentTheft || item.Type == Constants.IncidentNature)
                        {
                            Message = string.Format(GetMessageFromQnAMaker(Constants.ClaimProcess));

                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);
                            break;
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        }
                        else if (item.Type == Constants.ClaimProcess)
                        {
                            Message = string.Format(GetMessageFromQnAMaker(Constants.IncidentDetails));

                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);

                            break;
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        }
                        else if (item.Type == Constants.IncidentDetails)
                        {
                            //Upload the honesty pledge
                            //if (result.Query.ToString() == "Upload video")
                            if (result.Query.ToString() == "Upload the honesty pledge")
                            {
                                context.UserData.SetValue("AskForVideo", true);
                                //mc.strDoc1 = "IDET";
                                //string s = "IDET";
                                //mc.getDoc(ref mc.strDoc1);
                                WriteUpload("IDET");
                                Message = "";
                                // break;
                            }
                            else if (result.Query.ToString() == "Honesty Pledge Details Uploaded")
                            {
                                Message = string.Format(GetMessageFromQnAMaker(Constants.ScanPolicReport));

                                var splitSentance = regex.Split(Message);
                                strSplitSentnce = splitSentance[0];
                                actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                                Message = "";
                                await context.PostAsync(actvtyWithButtons);
                                context.Wait(this.MessageReceived);
                                break;
                            }
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        }
                        else if (item.Type == Constants.ScanPolicReport)
                        {
                            if (result.Query.ToString() == "Upload Report")
                            {
                                //context.UserData.SetValue("AskForPoliceReport", true);
                                //context.UserData.SetValue("AskForVideo", true);
                                WriteUpload("PREP");
                                Message = "";
                                //break;
                            }
                            else if (result.Query.ToString() == "Police Report Uploaded")
                            {
                                Message = string.Format(GetMessageFromQnAMaker(Constants.IncidentType));

                                var splitSentance = regex.Split(Message);
                                strSplitSentnce = splitSentance[0];
                                actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                                Message = "";
                                await context.PostAsync(actvtyWithButtons);
                                context.Wait(this.MessageReceived);
                                break;
                            }
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        }
                        else if (item.Type == Constants.IncidentType)
                        {
                            Message = String.Format(GetMessageFromQnAMaker(Constants.ImageQnA));
                            var splitedMessage = regex.Split(Message);
                            strSplitSentnce = splitedMessage[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);
                            break;
                            
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        }
                        else if (item.Type == Constants.ScanImage)
                        {
                            if (result.Query.ToString() == "Upload Image")
                            {
                                //userData.GetProperty<string>("AskForDocument") == "PREP"
                                //context.UserData.SetValue("AskForReceipt", true);
                                //context.UserData.SetValue("AskForPoliceReport", true);
                                //context.UserData.SetValue("AskForVideo", true);
                                WriteUpload("IMG");
                                Message = "";
                                break;
                            }
                            else if (result.Query.ToString() == "Image Uploaded")
                            {
                                Message = string.Format(GetMessageFromQnAMaker(Constants.ScanReceipts));

                                var splitSentance = regex.Split(Message);
                                strSplitSentnce = splitSentance[0];
                                actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                                Message = "";
                                await context.PostAsync(actvtyWithButtons);
                                context.Wait(this.MessageReceived);
                                break;
                            }
                        }
                        else if (item.Type == Constants.ScanReceipts)
                        {
                            if (result.Query.ToString() == "Upload Receipts")
                            {
                                //userData.GetProperty<string>("AskForDocument") == "PREP"
                                //context.UserData.SetValue("AskForReceipt", true);
                                //context.UserData.SetValue("AskForPoliceReport", true);
                                //context.UserData.SetValue("AskForVideo", true);
                                WriteUpload("RECP");
                                Message = "";
                                break;
                            }
                            else if (result.Query.ToString() == "Receipt Uploaded")
                            {
                                Message = string.Format(GetMessageFromQnAMaker(Constants.BankAccountNumber));
                            }

                            //var splitSentance = regex.Split(Message);
                            //strSplitSentnce = splitSentance[0];
                            //actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            //Message = "";
                            //await context.PostAsync(actvtyWithButtons);
                            //context.Wait(this.MessageReceived);
                            //break;
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        }
                        else if (item.Type == Constants.BankAccountNumber)
                        {

                            //Message = string.Format(GetMessageFromQnAMaker(Constants.Thankyou));
                            //Message = string.Format(GetMessageFromQnAMaker(Constants.WaitMessage));
                            Message = "";
                            //var reply = activity.CreateReply(activity.Text);
                            //await connector.Conversations.ReplyToActivityAsync(reply);
                            //break;
                            //var splitSentance = regex.Split(Message);
                            //strSplitSentnce = splitSentance[0];
                            //actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            //Message = "";
                            //await context.PostAsync(actvtyWithButtons);
                            //context.Wait(this.MessageReceived);
                            //break;
                            //context.UserData.SetValue("AskForFurtherDamage", true);
                            // context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                            //result.Query.;//string.Format(GetMessageFromQnAMaker(Constants.WaitMessage));
                            //var connector = new ConnectorClient(new Uri(activity));

                        }
                        else if (item.Type == Constants.WaitMessage)
                        {
                            Upload(); //Upload user response to FTP
                            System.Threading.Thread.Sleep(80000); 
                            Message = ReadFile("LP6C3AA13_ClaimStatus.txt"); //planning to add policy number as prefix with filename
                            break;
                        }

                        #region old code
                        //if (item.Type == Constants.IncidentTheft)
                        //{
                        //    Message = string.Format(GetMessageFromQnAMaker(Constants.PoliceComplaint), name);

                        //    var splitSentance = regex.Split(Message);
                        //    strSplitSentnce = splitSentance[0] + splitSentance[1];
                        //    actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                        //    Message = "";
                        //    await context.PostAsync(actvtyWithButtons);
                        //    context.Wait(this.MessageReceived);
                        //    context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        //}
                        //else if (item.Type == Constants.IncidentLoss)
                        //{
                        //    Message = string.Format(GetMessageFromQnAMaker(Constants.PoliceComplaint), name);

                        //    var splitSentance = regex.Split(Message);
                        //    strSplitSentnce = splitSentance[0] + splitSentance[1];
                        //    actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                        //    Message = "";
                        //    await context.PostAsync(actvtyWithButtons);
                        //    context.Wait(this.MessageReceived);
                        //    context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        //}
                        //else if (item.Type == Constants.IncidentDamaged)
                        //{
                        //    Message = string.Format(GetMessageFromQnAMaker(Constants.PoliceComplaint), name);

                        //    var splitSentance = regex.Split(Message);
                        //    strSplitSentnce = splitSentance[0] + splitSentance[1];
                        //    actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                        //    Message = "";
                        //    await context.PostAsync(actvtyWithButtons);
                        //    context.Wait(this.MessageReceived);
                        //    context.UserData.SetValue("AskForPoliceComplaintNumber", false);
                        //}
                        //else if (item.Type == Constants.IncidentStolen)
                        //{
                        //    Message = string.Format(GetMessageFromQnAMaker(Constants.PoliceComplaint), name);
                        //}
                        //else if (context.UserData.GetValue<bool>("AskForPoliceComplaintNumber") == false && item.Type == Constants.IsPoliceComplaint)
                        //{
                        //    Message = GetMessageFromQnAMaker(Constants.AskForPoliceComplaintNumber);
                        //}
                        //else if (context.UserData.GetValue<bool>("AskForPoliceComplaintNumber") == true && item.Type == Constants.IsPoliceComplaint)
                        //{
                        //    //Message = GetMessageFromQnAMaker(Constants.AskForPoliceComplaintNumber);
                        //    InsuredPremises = result.Query.ToString();
                        //    Message = GetMessageFromQnAMaker(Constants.IncidentLocation);
                        //}
                        //else if (item.Type == Constants.IsNoPoliceComplaint)
                        //{
                        //    Message = GetMessageFromQnAMaker(Constants.PolicyAvailable);
                        //    resetContext(context);
                        //}
                        //else if (context.UserData.ContainsKey("AskForPoliceComplaintNumber") && item.Type == Constants.PoliceReportNumber)
                        //{
                        //    // Message = GetMessageFromQnAMaker(Constants.PolicyAvailable);
                        //    //resetContext(context);

                        //    Message = GetMessageFromQnAMaker(Constants.IncidentDate);
                        //    context.UserData.SetValue("AskForPoliceComplaintNumber", true);
                        //    context.UserData.SetValue("AskForIncidentDate", false);

                        //    //resetContext(context);
                        //    break;
                        //}
                        //New Questions Added
                        //else if (item.Type == Constants.PolicyNumber)
                        //{
                        //    PolicyNumber = result.Query.ToString();
                        //    Message = GetMessageFromQnAMaker(Constants.IncidentDate);
                        //    break;
                        //}
                        //else if (context.UserData.ContainsKey("AskForIncidentDate") && item.Type == Constants.IncidentDate)
                        ////else if (item.Type == Constants.IncidentDate)
                        //{
                        //    IncidentDate = result.Query.ToString();

                        //    Message = GetMessageFromQnAMaker(Constants.InsuredLocation);
                        //    var splitSentance = regex.Split(Message);
                        //    strSplitSentnce = splitSentance[0];
                        //    actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                        //    Message = "";
                        //    await context.PostAsync(actvtyWithButtons);
                        //    context.Wait(this.MessageReceived);
                        //    context.UserData.SetValue("AskForIncidentDate", true);
                        //    //resetContext(context);
                        //    break;
                        //}
                        //else if (item.Type == Constants.InsuredLocation)
                        //{
                        //    InsuredPremises = result.Query.ToString();

                        //    //Message = GetMessageFromQnAMaker(Constants.DamageEstimate);
                        //    // Message = GetMessageFromQnAMaker(Constants.InsuredLocation);
                        //    Message = GetMessageFromQnAMaker(Constants.IncidentLocation);
                        //    //EstimatedDamage = result.Query.ToString();

                        //    break;
                        //}
                        //else if (item.Type == Constants.IncidentLocation)
                        //{

                        //    IncidentLocation = result.Query.ToString();
                        //    Message = GetMessageFromQnAMaker(Constants.DamageEstimate);
                        //    break;

                        //}
                        //else if (item.Type == Constants.InsuredLocation)
                        //{
                        //    InsuredPremises = result.Query.ToString();
                        //    Message = GetMessageFromQnAMaker(Constants.DamageEstimate);
                        //    break;
                        //}

                        //else if (item.Type == Constants.DamageEstimate)
                        //{
                        //    EstimatedDamage = result.Query.ToString();
                        //    FTPUpload(); //Upload user response to FTP
                        //                 //Message = GetMessageFromQnAMaker(Constants.WaitMessage);
                        //                 //await context.PostAsync(Message);
                        //                 //context.Wait(this.MessageReceived);

                        //    System.Threading.Thread.Sleep(80000);
                        //    Message = ReadFile("FinalMessage.txt");
                        //    break;
                        //}
                        #endregion
                        else
                        {
                            Message = GetMessageFromQnAMaker(Constants.Help);
                        }
                    }
                    //}
                    //else
                    //{
                    //    Message = GetMessageFromQnAMaker(Constants.ValidPolicyNumber);
                    //}
                    //    }
                    //    else
                    //    {
                    //        Message = GetMessageFromQnAMaker(Constants.ValidPhoneNumber);
                    //    }
                }
                else
                {
                    Message = GetMessageFromQnAMaker(Constants.Help);
                    resetContext(context);
                }
                if (Message.Length > 0)
                {
                    await context.PostAsync(Message);
                    context.Wait(this.MessageReceived);
                   
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        async Task<PolicyDetails> GetuserDetails(string columnName, string message)
        {
            var userData = new PolicyDetails();
            var path = AppDomain.CurrentDomain.BaseDirectory;
            using (CsvReader csvReader = new CsvReader(new StreamReader(Path.Combine(path, "UserList.csv")), hasHeaders: true))
            {
                int nameColumnIndex = csvReader.GetFieldIndex(columnName);
                var length = csvReader.FieldCount;

                while (csvReader.ReadNextRecord())
                {
                    if (csvReader[nameColumnIndex] == message)
                    {
                        userData.Name = csvReader[1];
                        userData.MobileNumber = csvReader[3];
                        userData.PolicyNumber = csvReader[2];
                        userData.PolicyReportNumber = csvReader[0];
                        return userData;
                    }
                }

            }
            return null;
        }

        private string GetMessageFromQnAMaker(string entity)
        {
            var postBody = $"{{\"question\": \"{entity}\"}}";
            using (WebClient client = new WebClient())
            {
                client.Encoding = System.Text.Encoding.UTF8;
                //client.Headers.Add("Ocp-Apim-Subscription-Key", "6f0fd029ba0845c981a1dd93f5b41d51");
                //client.Headers.Add("Ocp-Apim-Subscription-Key", "1e692318045c4563a2a244f3f88df60d");
                client.Headers.Add("Ocp-Apim-Subscription-Key", "14814bb73e4e4404a1461d0bee2dd3a2");

                client.Headers.Add("Content-Type", "application/json");
                var answer = JsonConvert.DeserializeObject<QnAMakerResult>(
                 client.UploadString("https://westus.api.cognitive.microsoft.com/qnamaker/v1.0/knowledgebases/cf1faf4f-b83e-49df-bf66-11b838a22752/generateAnswer", postBody));

                if (answer.Score > 0.2)
                {
                    return WebUtility.HtmlDecode(answer.Answer);
                }
            }
            return string.Empty;
        }
        private void resetContext(IDialogContext context)
        {
            try
            {
                context.UserData.RemoveValue("Name");
                context.UserData.RemoveValue("AskedForMobileNumber");
                //context.UserData.RemoveValue("MobileNumber");
                //context.UserData.RemoveValue("PolicyNumber");
                //context.UserData.RemoveValue("AskedForPolicyNumber");
                context.UserData.RemoveValue("AskedGreetings");
                context.UserData.RemoveValue("AskForPoliceComplaintNumber");
                context.UserData.RemoveValue("AskForIncidentDate");
                context.UserData.RemoveValue("AskForFurtherDamage");
                context.UserData.RemoveValue("AskForVideo");
                context.UserData.RemoveValue("AskForPoliceReport");
                context.UserData.RemoveValue("AskForReceipt");

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static List<CardAction> CreateButtons(string strMessage)
        {
            List<CardAction> cardButtons = new List<CardAction>();
            MatchCollection collectionMatches = Regex.Matches(strMessage, "<li(.*?)>(.*?)</li>");
            var regExpression = new System.Text.RegularExpressions.Regex("((<li.*>)(.*)(</li>))");
            foreach (Match strMatch in collectionMatches)
            {
                var subTextValue = regExpression.Match(strMatch.ToString());
                CardAction CardButton = new CardAction()
                {
                    Title = subTextValue.Groups[3].ToString(),
                    Value = subTextValue.Groups[3].ToString()
                };
                cardButtons.Add(CardButton);
            }

            return cardButtons;
        }
        private static Activity fnCreateButtons(IDialogContext context, string strMessage, string strInitSent = "")
        {
            Activity actWithButtons = (Activity)context.MakeMessage();
            List<CardAction> cardButtons = new List<CardAction>();
            Attachment plAttachment = new Attachment();

            try
            {
                actWithButtons.Recipient = actWithButtons.Recipient;
                actWithButtons.Type = "message";
                //var splitSentance = regex.Split(strMessage);

                cardButtons = CreateButtons(strMessage);

                HeroCard plCard = new HeroCard()
                {
                    Text = strInitSent,
                    Buttons = cardButtons
                };
                plAttachment = plCard.ToAttachment();
                actWithButtons.Attachments.Add(plAttachment);
                actWithButtons.AttachmentLayout = "carousel";
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return actWithButtons;
        }

        public void CreateCSV(string FilePath, string FileName, DataTable dt)
        {
            StringBuilder sb = new StringBuilder();

            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                    sb.Append(dr[dc.ColumnName].ToString() + ",");
                sb.Remove(sb.Length - 1, 1);
                sb.AppendLine();
            }
            File.WriteAllText(Path.Combine(FilePath, FileName), sb.ToString());
        }

        public void WriteUpload(string str)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            File.WriteAllText(path + "//uploaddoc.txt", str);
        }

        private string ReadFile(string txtFile)
        {
            string text;

            var path = AppDomain.CurrentDomain.BaseDirectory;

            if (File.Exists(Path.Combine(path + "//RPAFiles//", txtFile)))
            {
                var fileStream = new FileStream((Path.Combine(path + "//RPAFiles//", txtFile)), FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    return text = streamReader.ReadToEnd();
                }
            }
            else
            {
                return text = GetMessageFromQnAMaker(Constants.ClaimStatus);
            }
        }
        private void Upload()
        {
            DataTable dt = GetData();

            string fileName = "chatbotoutput.csv";

            string path = path = AppDomain.CurrentDomain.BaseDirectory;

            CreateCSV(path + "//BotFiles//", fileName, dt);
        }
        private DataTable GetData()
        {
            DataTable dt = new DataTable();
            //InsuredName,PolicyNumber,Email
            //"Kevin",LP6C3AA13,sivagnanam.v@rapidvaluesolutions.com


            dt.Columns.Add("InsuredName", typeof(string));
            dt.Columns.Add("PolicyNumber", typeof(string));
            dt.Columns.Add("Email", typeof(string));

            dt.Rows.Add("InsuredName", "PolicyNumber", "Email"); //Header Row
            dt.Rows.Add("Kevin", "LP6C3AA13", "sivagnanam.v@rapidvaluesolutions.com"); //Data Row

            //dt.Columns.Add("InsuredName", typeof(string));
            //dt.Columns.Add("PolicyNumber", typeof(string));
            //dt.Columns.Add("IncidentDate", typeof(string));
            //dt.Columns.Add("IncidentLocation", typeof(string));
            //dt.Columns.Add("InsuredPremises", typeof(string));
            //dt.Columns.Add("EstimatedDamage", typeof(string));
            //dt.Rows.Add("InsuredName", "PolicyNumber", "IncidentDate", "InsuredPremises", "IncidentLocation", "EstimatedDamage"); //Header Row
            //dt.Rows.Add(InsuredName, PolicyNumber, IncidentDate, InsuredPremises, IncidentLocation, EstimatedDamage); //Data Row

            return dt;
        }



        private async Task SendResponseToImage(Activity activity)

        {

            ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));




            var reply = activity.CreateReply();

            reply.Attachments.Clear();

            reply.Attachments.Add(new Attachment()

            {

                ContentUrl = $"UrlImage.GIF",

                ContentType = "image/gif",

                Name = "NameImage.gif"

            });

            await connector.Conversations.ReplyToActivityAsync(reply);

        }


        //Sample Testing Code
        [LuisIntent("shopkart")]
        public async Task ShopKartTask(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            var message = await activity;
            string strSplitSentnce = "";
            Regex regex = new Regex("</br>");
            EntityRecommendation mandClaim = new EntityRecommendation();
            mandClaim.Entity = Constants.Claim;
            Activity actvtyWithButtons = (Activity)context.MakeMessage();

            if (result.Entities != null && result.Entities.Count > 0)
            {
                if (context.UserData.ContainsKey("AskedGreetings") || result.Entities[0].Entity.ToUpper() == mandClaim.Entity.ToUpper())
                {
                    foreach (var item in result.Entities)
                    {
                        if (item.Type == Constants.Claim)
                        {
                            Message = GetMessageFromQnAMaker(Constants.Claim1);
                            if (!context.UserData.ContainsKey("AskedGreetings"))
                            {
                                context.UserData.SetValue("AskedGreetings", true);
                            }
                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);

                            context.UserData.SetValue("AskForPoliceComplaintNumber", false);

                            break;


                        }
                        else if (item.Type == Constants.PhoneNumber)
                        {
                            Message = string.Format(GetMessageFromQnAMaker(Constants.IncidentStuff));

                            var splitSentance = regex.Split(Message);
                            strSplitSentnce = splitSentance[0];
                            actvtyWithButtons = fnCreateButtons(context, Message, strSplitSentnce);
                            Message = "";
                            await context.PostAsync(actvtyWithButtons);
                            context.Wait(this.MessageReceived);
                            context.UserData.SetValue("AskedForMobileNumber", true);
                        }
                    }
                }
                else
                {
                    Message = GetMessageFromQnAMaker(Constants.HelpContent);
                    resetContext(context);
                }
            }
            if (Message.Length > 0)
            {
                await context.PostAsync(Message);
                context.Wait(this.MessageReceived);
            }
        }






    }






}
