﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadContent.aspx.cs" Inherits="LuisBot.UploadContent" %>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="//code.jquery.com/jquery.js"></script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <%--<script>
   $('#FileUpload2').click(function(event){
    event.preventDefault()});
  </script>--%>
            <script language="javascript" type="text/javascript">

                function UploadFileNow() {

                    //var value = $("#FileUpload2").val();

                    $('#FileUpload2').click(function (event) {
                        event.preventDefault()
                    });

                    if (value != '') {

                        $("#form1").submit();

                    }

                };
            </script>

            <div>

                <h2>Upload file without Submit button</h2>

                <asp:Button ID="btn_test" runat="server" Text="Test" data-target="#myModal" data-toggle="modal" CssClass="btn btn-primary" />



                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="Green" />

                                Select file and upload will start automatically

                <h5>Change Profile Picture</h5>


                                <asp:FileUpload ID="FileUpload2" runat="server" ClientIDMode="Static" onchange="UploadFileNow()" />

                                <br />
                                <br />

                                <asp:Label ID="lbl_profilepicturePreview" runat="server" Text="The preview of your updated profile picture: "></asp:Label>
                                <asp:Image ID="img" runat="server" />
                                <br />
                                <asp:Label ID="lbl_profilepicture" runat="server"></asp:Label>
                                <br />

                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="btn_profilepicturesubmit" runat="server" CssClass="btn btn-primary" Text="Save Profile Picture" />
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

            </div>
        </div>
    </form>
</body>
</html>
