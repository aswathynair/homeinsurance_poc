﻿namespace LuisBot
{
    using System;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Http;
    using Dialogs;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Connector;
    using Services;
    using System.Linq;
    using System.IO;
    using System.Web.UI;
    using System.Text;

    [BotAuthentication]

    public class MessagesController : ApiController
    {
        private static readonly bool IsSpellCorrectionEnabled = bool.Parse(WebConfigurationManager.AppSettings["IsSpellCorrectionEnabled"]);

        private readonly BingSpellCheckService spellService = new BingSpellCheckService();
        PolicyDetails pc = new PolicyDetails();
        // public string strDoc { get; set; }
        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            if (activity.Type == ActivityTypes.Message)
            {
                if (activity.Text != null)
                {
                    if (IsSpellCorrectionEnabled)
                    {
                        try
                        {
                            activity.Text = await this.spellService.GetCorrectedTextAsync(activity.Text);
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError(ex.ToString());
                        }
                    }
                }
                if (activity.Attachments?.Any() == true)
                {
                    var connector = new ConnectorClient(new Uri(activity.ServiceUrl));
                    var message = string.Empty;
                    // get file as Stream
                    var fileUrl = activity.Attachments[0].ContentUrl;
                    var fileName = activity.Attachments[0].Name;
                    //activity.Attachments[0].
                    //var client = new HttpClient();
                    //var photoStream = await client.GetStreamAsync(fileUrl);

                    string path = AppDomain.CurrentDomain.BaseDirectory;
                    WebClient wclient = new WebClient();
                    //client1.DownloadFile(new Uri(fileUrl), @"D:\Siva\" + fileName);
                    //IDialogContext context; 
                    //ReadFile();
                    //string str = pc.getDoc(ref pc.strDoc1);
                    string strDoc = ReadFile();
                    //activity.Text = "Upload failed, please try again.";
                    string policynumber = "LP6C3AA13_";
                    //if (userData.GetProperty<bool>("AskForVideo") == true)
                    //if (userData.GetProperty<bool>("AskForVideo") == true)
                    if (strDoc == "IDET")
                    {
                        wclient.DownloadFile(new Uri(fileUrl), path + "\\IncidentDetails\\" + policynumber + fileName);
                        activity.Text = "Honesty Pledge Details Uploaded";
                        //activity.CreateReply(activity.Text);
                        //var reply = activity.CreateReply(activity.Text);
                        //await connector.Conversations.ReplyToActivityAsync(reply);
                    }
                    //else if (userData.GetProperty<bool>("AskForVideo") == true && userData.GetProperty<bool>("AskForPoliceReport") == false)
                    //else if (userData.GetProperty<bool>("AskForPoliceReport") == true)
                    if (strDoc == "PREP")
                    {
                        wclient.DownloadFile(new Uri(fileUrl), path + "\\ClaimsPoliceReports\\" + policynumber + "PoliceReport.pdf");
                        activity.Text = "Police Report Uploaded";
                        //activity.CreateReply(activity.Text);
                    }

                    //else if (userData.GetProperty<bool>("AskForVideo") == true && userData.GetProperty<bool>("AskForPoliceReport") == true && userData.GetProperty<bool>("AskForReceipt") == false)
                    //else if (userData.GetProperty<bool>("AskForReceipt") == true)
                    if (strDoc == "RECP")
                    {
                        wclient.DownloadFile(new Uri(fileUrl), path + "\\ClaimsReceipts\\" + policynumber + "Receipt.pdf");
                        activity.Text = "Receipt Uploaded";
                        //activity.CreateReply(activity.Text);
                        //var reply = activity.CreateReply(activity.Text);
                        //await connector.Conversations.ReplyToActivityAsync(reply);
                    }

                    if (strDoc == "IMG")
                    {
                        wclient.DownloadFile(new Uri(fileUrl), path + "\\ClaimsReceipts\\" + policynumber +"_"+ fileName);
                        activity.Text = "Image Uploaded";
                        //activity.CreateReply(activity.Text);
                        //var reply = activity.CreateReply(activity.Text);
                        //await connector.Conversations.ReplyToActivityAsync(reply);
                    }

                    var reply = activity.CreateReply(activity.Text);
                    await connector.Conversations.ReplyToActivityAsync(reply);
                    //message = activity.Text;
                    //await connector.Conversations.ReplyToActivityAsync(reply);
                    //await Conversation.SendAsync(activity, () => new RootLuisDialog());
                    //activity.Text = "";
                    // await context.PostAsync(activity.Text);
                    //context.Wait(activity.Text);
                }
                await Conversation.SendAsync(activity, () => new RootLuisDialog());
            }
            else if (activity.Type == ActivityTypes.DeleteUserData)
            {
                StateClient sc = activity.GetStateClient();
                BotData userData = sc.BotState.GetPrivateConversationData(activity.ChannelId, activity.Conversation.Id, activity.From.Id);
                userData.SetProperty<string>("Name", "");
                userData.SetProperty<bool>("AskedForMobileNumber", false);
                userData.SetProperty<bool>("AskedGreetings", false);
                //userData.SetProperty<bool>("MobileNumber", false);
                //userData.SetProperty<bool>("PolicyNumber", false);
                //userData.SetProperty<bool>("AskedForPolicyNumber", false);
                userData.SetProperty<bool>("AskForPoliceComplaintNumber", false);
                userData.SetProperty<bool>("AskForIncidentDate", false);
                userData.SetProperty<bool>("AskForFurtherDamage", false);
                userData.SetProperty<bool>("AskForVideo", false);
                userData.SetProperty<bool>("AskForPoliceReport", false);
                userData.SetProperty<bool>("AskForReceipt", false);

                sc.BotState.SetPrivateConversationData(activity.ChannelId, activity.Conversation.Id, activity.From.Id, userData);
                ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
                Activity replyMessage = activity.CreateReply("Personal data has been deleted.");
            }
            else
            {
                this.HandleSystemMessage(activity);
            }
            //activity.Type=ActivityTypes.

            var response = Request.CreateResponse(HttpStatusCode.OK);
            if (activity.Text == "00678912340056")
            {
                ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
                activity.Text = "Thanks for your details! Please hold on while we retrieve your claim status.";
                var reply = activity.CreateReply(activity.Text);
                await connector.Conversations.ReplyToActivityAsync(reply);
                await Conversation.SendAsync(activity, () => new RootLuisDialog());
            }
            return response;
        }

        private Activity HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Ping)
            {

            }

            return null;
        }



        private bool getContext(IDialogContext context)
        {
            return context.UserData.GetValue<bool>("AskForVideo");
        }

        private string ReadFile()
        {
            string text;

            var path = AppDomain.CurrentDomain.BaseDirectory;

            //if (File.Exists(Path.Combine(path + "//RPAFiles//", "FinalMessage.txt")))
            //{
            var fileStream = new FileStream(path + "//uploaddoc.txt", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                return text = streamReader.ReadToEnd();
            }
            //}

        }
    }
}